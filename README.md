# Lendi - Custom Object with Email Alert

Salesforce project to handle customer complaints.

## Assumption

For the purpose of this task, the org has three business units:

1. Customer Service Team
2. Home Loan Consultant Team
3. Compliance Team

The customer service team is responsible for taking complaints on behalf of the end-users (e.g via phone/chat). An email is sent to the customer when a complaint is created in the system.

## Complaints - Access Level


| Business Unit             	| Create Complaints 	| Read Complaints 	| Edit Complaints 	| Delete Complaints 	|
|---------------------------	|-------------------	|-----------------	|-----------------	|-------------------	|
| Customer Service Team     	| Yes               	| Yes             	| Yes             	| Yes               	|
| Home Loan Consultant Team 	| No                	| No              	| No              	| No                	|
| Compliance Team           	| No                	| Yes             	| No              	| No                	|

## Development

### Prerequisites

Salesforce account with dev hub enabled. It's assumed the environment that runs the following snippet has salesforce cli and credentials configured.

### Script

```sh
sfdx force:org:create -f config/project-scratch-def.json --setalias lendi --durationdays 7 --setdefaultusername --json --loglevel fatal

sfdx force:source:push -u lendi

sfdx force:data:tree:import -p data/sample-data-plan.json -u lendi

sfdx force:org:open -u lendi

```

> Only sample users has been imported into the system, and this setup can be extended to seed user accounts and complaints. e.g.
`sfdx force:user:create -u lendi --definitionfile config/user-customer-service.json`

> Some of the hardcoded alias, test emails can be parametrised and injected as part of the CI/CD process.

## Salesforce Org Setup

![Setup - custom](img/sf-custom.png)

1. Custom Object: Customer Complaints, with customer name (lookup from contact object), email (formula field), mobile (formula field), bank name (picklist) loan application number (text), complaints category (picklist),  complaint details (rick text), status (picklist) and complaint number (auto-generated: CC-XXX format).

2. Validation Rule: to check customer name before saving the complaints form.

![Setup - sf-complaint-form-validation-error.png](img/sf-complaint-form-validation-error.png)

3. Standard object: Email and mobile are required fields in the contact object.

![Setup - sf-customer-complaint](img/sf-customer-complaint.png)

4. Email Template

5. Workflow Rule: Created a workflow rule to send email to the customer every time a complaint is logged. 

![Setup - sf-complaint-email](img/sf-complaint-email.png)

6. Profiles

### Access

>> Customer Service - Access

![Setup - sf-customer-service-create](img/sf-customer-service-create.png)

![Setup - sf-customer-service-list](img/sf-customer-service-list.png)

![Setup - sf-customer-service-view](img/sf-customer-service-view.png)

>> Compliance Team - Access

![Setup - sf-compliance-view](img/sf-compliance-view.png)

![Setup - sf-compliance-no-edit](img/sf-compliance-no-edit.png)

>> Home Loan Consultant Team - Access

![Setup - sf-home-loan-no-access-to-complaints.png](img/sf-home-loan-no-access-to-complaints.png)
